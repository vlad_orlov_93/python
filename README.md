To run project its necessary to implement next commands from root directory:
  - pip3 install .
  - cd src
  - python3 demo.py


In order to build project its necessary to run next command from the folder where pyproject.toml is located:
   - python3 -m build 
After that folder 'dist' is made in which whl and tr.gz files are located.   